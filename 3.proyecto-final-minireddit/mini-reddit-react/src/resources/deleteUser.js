import { host } from '../config';
import { throwError } from '../components/Utilities';

const deleteUser = async (oldPwd, token) => {
  const requestOptions = {
    method: 'PUT',
    headers: { 'Content-Type': 'application/json', Authorization: token },
    body: JSON.stringify({ oldPwd }),
  };
  let res = await fetch(`${host}/user/delete`, requestOptions);
  const bodyres = await res.json();

  if (res.ok) {
    return bodyres;
  } else {
    throwError(res.status, bodyres.message);
  }
};

export { deleteUser };
