import { throwError } from '../components/Utilities';
import { host } from '../config';

const deletePost = async (postId, token) => {
  const requestOptions = {
    method: 'DELETE',
    headers: { 'Content-Type': 'application/json', Authorization: token },
  };
  const resVote = await fetch(`${host}/post/${postId}`, requestOptions);
  const bodyVote = await resVote.json();
  if (resVote.ok) {
    return resVote;
  } else {
    throwError(resVote.status, bodyVote.message);
  }
};

export { deletePost };
