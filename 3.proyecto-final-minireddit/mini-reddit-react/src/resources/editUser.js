import { host } from '../config';
import { throwError } from '../components/Utilities';

const editUser = async (formData, token) => {
  const requestOptions = {
    method: 'PUT',
    headers: { Authorization: token },
    body: formData,
  };
  const resLogin = await fetch(`${host}/user/edit`, requestOptions);
  const resBody = await resLogin.json();

  if (resLogin.ok) {
    return resBody;
  } else {
    throwError(resLogin.status, resBody.message);
  }
};

export { editUser };
