# Portal genérico de opiniones.

Implementar una API que permita publicar opiniones y que otras personas puedan verlas.

## Dependencias desarrollo

-   eslint // aviso de errores en el codigo
-   prettier // gestion de errores
-   nodemon // carga cambios automaticos en el servidor tras guardar

## Dependencias producción

-   express // simulacion de servidor
-   dotenv // acceso al archivo .env
-   morgan // imprime las peticiones en consola
-   mysql2 // da acceso a mysql para las tablas
-   jsonwebtoken // gestion del token json
-   cors ?
-   uuid ?
-   joi ?
-   express-fileupload ? // acceso a files
-   sharp ? // procesamiento de imagenes
-   bcrypt ? // encriptacion de datos sensibles

## Instalar

-   Crear una base de datos vacía en una instancia de MySQL local.

-   Guardar el archivo `.env.example` como `.env` y cubrir los datos necesarios.

-   Ejecutar `npm run initDB` para crear las tablas necesarias en la base de datos anteriormente creada.

-   Ejecutar `npm run dev` o `npm start` para lanzar el servidor.

## Entidades

-   Users:

    -   id
    -   email
    -   password
    -   createdAt
    -   modifiedAt

-   Post:

    -   id
    -   idUser
    -   text
    -   image (opcional)
    -   createdAt
    -   modifiedAt

-   Votos:

-   id
-   boolean 0-1
-   user_id
-   tweet_id

## Endpoints

Usuarios:

-   POST [/user] - Registro de usuario
-   POST [/user/login] - Login de usuario (devuelve token)
-   GET [/user] - Devuelve información del usuario QUE FIGURA EN EL TOKEN (necesita cabecera con token)
-   PATCH [/user/edit] - modificar información del usuario

Posts:

-   POST [/post] - Permite crear un post (necesita cabecera con token)
-   GET [/post] - Lista todos los post
-   GET [/post/:id] - Devuelve un post

Votes:

-   POST [/post/:id/vote] - permite votar

## OPCIONAL: (temática)

Usuarios:

-   GET [/user/:id] - ver perfiles de otros usuarios
-   DELETE [/user/delete] - eliminar usuario

Posts:

-   Añadir la opción de postear una imagen
-   PATCH [/post/:id/edit] - modificar post
-   DELETE [/post/:id/delete] - eliminar post

Votos:

-   GET [/post/:id/votes] - saber la media de votos de un post
-   PATCH [/post/:id/vote] - modificar un voto
-   DELETE [/post/:id/vote/delete] - eliminar voto
