const getDB = require('../../getConnection');

const deleteTweetQuery = async (idTweet) => {
    let connection;

    try {
        connection = await getDB();

        await connection.query(`DELETE FROM tweets WHERE id = ?`, [idTweet]);
    } finally {
        if (connection) connection.release();
    }
};

module.exports = deleteTweetQuery;
