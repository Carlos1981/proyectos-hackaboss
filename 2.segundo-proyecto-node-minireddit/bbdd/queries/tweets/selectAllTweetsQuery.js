const getDB = require('../../getConnection');

const selectAllTweetsQuery = async (keyword = '') => {
    let connection;

    try {
        connection = await getDB();

        const [tweets] = await connection.query(
            `SELECT * FROM tweets WHERE text LIKE ? ORDER BY createdAt DESC`,
            [`%${keyword}%`]
        );

        return tweets;
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectAllTweetsQuery;
