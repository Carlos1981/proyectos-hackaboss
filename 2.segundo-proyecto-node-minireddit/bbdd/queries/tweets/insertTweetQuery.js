const getDB = require('../../getConnection');

const insertTweetQuery = async (text, image, idUser) => {
    let connection;

    try {
        connection = await getDB();

        await connection.query(
            `
                INSERT INTO tweets (text, image, idUser, createdAt)
                VALUES (?, ?, ?, ?)
            `,
            [text, image, idUser, new Date()]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertTweetQuery;
