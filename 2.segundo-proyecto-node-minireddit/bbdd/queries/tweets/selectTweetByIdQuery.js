const getDB = require('../../getConnection');

const { generateError } = require('../../../helpers');

const selectTweetByIdQuery = async (idTweet) => {
    let connection;

    try {
        connection = await getDB();

        const [tweets] = await connection.query(
            `SELECT * FROM tweets WHERE id = ?`,
            [idTweet]
        );

        if (tweets.length < 1) {
            generateError('Tweet no encontrado', 404);
        }

        return tweets[0];
    } finally {
        if (connection) connection.release();
    }
};

module.exports = selectTweetByIdQuery;
