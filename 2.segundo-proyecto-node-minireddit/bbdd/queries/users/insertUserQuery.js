const getDB = require('../../getConnection');
const bcrypt = require('bcrypt');

const { generateError } = require('../../../helpers');

const insertUserQuery = async (email, password) => {
    let connection;

    try {
        connection = await getDB();

        // Comprobamos si existe algún usuario con el email que del body.
        const [users] = await connection.query(
            `SELECT id FROM users WHERE email = ?`,
            [email]
        );

        // Si el email ya está pillado lanzamos un error.
        if (users.length > 0) {
            generateError('Ya existe un usuario con ese email', 409);
        }

        // Encriptamos la contraseña.
        const hashedPass = await bcrypt.hash(password, 10);

        // Insertamos el usuario.
        await connection.query(
            `INSERT INTO users (email, password, createdAt) VALUES (?, ?, ?)`,
            [email, hashedPass, new Date()]
        );
    } finally {
        if (connection) connection.release();
    }
};

module.exports = insertUserQuery;
