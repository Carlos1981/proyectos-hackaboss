'use strict';

require('dotenv').config();
const express = require('express');
const morgan = require('morgan');
const path = require('path');
const { PORT } = process.env;

// Creamos el servidor.
const app = express();

/* #####################
 ####CONTROLADORES USER##
 #########################*/

const {
    newUser,
    loginUser,
    getOwnUser,
    editUser,
} = require('./controllers/users'); // controlador

// Crear un nuevo usuario.
app.post('/user', newUser); // esto es un middleware el conjunto de funciones

// Login de usuario.
app.post('/user/login', loginUser); // users/login es el end-point donde va la peticion

// Obtener información del usuario del token.
app.get('/user', getOwnUser);

// Editar información del usuario:
app.patch('/user/edit', editUser);

/* #####################
 ####CONTROLADORES POST##
 #########################*/
const { getPost, listPost, newPost } = require('./controllers/posts'); // controlador

// Crear un nuevo post.
app.post('/post', newPost);

// Obtener información del post.
app.get('/post/:id', getPost);

// lista de posts.
app.get('/post', listPost);

/* #####################
 ####CONTROLADORES Votes##
 #########################*/

const newVote = require('./controllers/votes/newVote');

// Creamos un nuevo voto
app.post('/post/:id/vote', newVote);

/* #####################
 ####MIDDLEWARE DE ERRORES##
 #########################*/

// Middleware de error.
app.use((err, req, res, next) => {
    console.error(err);

    res.status(err.statusCode || 500).send({
        status: 'error',
        message: err.message,
    });
});

// Middleware de ruta no encontrada.
app.use((req, res) => {
    res.status(404).send({
        status: 'error',
        message: 'Ruta no encontrada',
    });
});

//-------------------*************-------------------///
// Ponemos el servidor a escuchar peticiones en un puerto dado.
app.listen(process.env.PORT, () => {
    console.log(`Server listening at http://localhost:${process.env.PORT}`);
});
