const newPost = require('./newPost');
const listPost = require('./listPost');
const getPost = require('./getPost');

module.exports = {
    newPost,
    listPost,
    getPost,
};
