/* const insertTweetQuery = require('../../bbdd/queries/tweets/insertTweetQuery');

const { generateError, savePhoto } = require('../../helpers'); */

const newPost = async (req, res, next) => {
    try {
        /*     const { text } = req.body;

        if (!text) {
            generateError('Faltan campos', 400);
        }

        // Variable donde almacenaremos el nombre de la imagen (si existe);
        let imgName;

        // Comprobamos si existe una imagen.
        if (req.files?.image) {
            // Guardamos la imagen en la carpeta "uploads" y obtenemos el nombre de la misma.
            imgName = await savePhoto(req.files.image);
        }

        // Insertamos el nuevo tweet.
        await insertTweetQuery(text, imgName, req.user.id); */

        res.send({
            status: 'ok',
            message: 'Post creado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = newPost;
