const newUser = require('./newUser');
const loginUser = require('./loginUser');
const getOwnUser = require('./getOwnUser');
const editUser = require('./editUser');

module.exports = {
    newUser,
    loginUser,
    getOwnUser,
    editUser,
};
