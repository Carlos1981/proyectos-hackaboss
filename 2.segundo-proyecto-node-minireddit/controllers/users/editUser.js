'use strict';
//const insertUserQuery = require('../../bbdd/queries/users/insertUserQuery');

// const { generateError } = require('../../helpers');

const editUser = async (req, res, next) => {
    try {
        // Obtenemos los campos del body.
        //  const { email, password } = req.body;

        /*     // Si faltan campos lanzamos un error.
        if (!email || !password) {
            generateError('Faltan campos', 400);
        }

        // Insertamos el usuario en la base de datos.
      await insertUserQuery(email, password); */

        res.send({
            status: 'ok',
            message: 'Usuario modificado',
        });
    } catch (err) {
        next(err);
    }
};

module.exports = editUser;
